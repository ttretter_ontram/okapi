/*===========================================================================
  Copyright (C) 2021 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.steps.transliteration;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.UsingParameters;
import net.sf.okapi.common.pipeline.BasePipelineStep;
import net.sf.okapi.common.pipeline.annotations.StepParameterMapping;
import net.sf.okapi.common.pipeline.annotations.StepParameterType;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextPart;

@UsingParameters(Parameters.class)
public class TransliterationStep extends BasePipelineStep {
	private Parameters params = new Parameters();
	private LocaleId targetLocale;

	@StepParameterMapping(parameterType = StepParameterType.TARGET_LOCALE)
	public void setTargetLocale(LocaleId targetLocale) {
		this.targetLocale = targetLocale;
	}

	@Override
	public String getName() {
		return "Text Transliteration";
	}

	@Override
	public String getDescription() {
		return "Transliterates the text units content of a document."
				+ " Expects: filter events. Sends back: filter events.";
	}

	@Override
	public Parameters getParameters() {
		return params;
	}

	@Override
	public void setParameters(IParameters params) {
		this.params = (Parameters) params;
	}

	@Override
	protected Event handleTextUnit(Event event) {
		ITextUnit tu = event.getTextUnit();

		// Skip non-translatable
		if (!tu.isTranslatable())
			return event;

		if (params == null || !params.isValid())
			return event;

		LocaleId toTargetLocale = params.getToTargetLocale();
		if (!toTargetLocale.equals(this.targetLocale))
			return event;

		LocaleId fromTargetLocale = params.getFromTargetLocale();
		TextContainer tc = tu.getTarget(fromTargetLocale);
		if (tc == null || !tc.hasText())
			return event;

		for (TextPart part : tc.getParts()) {
			String oldText = part.text.getCodedText();
			String newText = params.getTransliterator().transliterate(oldText);
			part.text.setCodedText(newText);
		}
		tu.setTarget(toTargetLocale, tc);
		tu.removeTarget(fromTargetLocale);

		return event;
	}
}
