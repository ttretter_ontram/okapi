/*===========================================================================
  Copyright (C) 2023 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/
package net.sf.okapi.lib.segmentation;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;


@RunWith(JUnit4.class)
public class RomanianSRXTest {
	private static OkapiSegmenter segmenter;

	@BeforeClass
	public static void init() {
		segmenter = new OkapiSegmenter("ro");
	}

	@Test
	public void testOkapiSegmentTest() {
		segment();
	}

	private void segment() {
		test("Aceasta este o propozitie fara diacritice. ");
		test("Aceasta este o fraza fara diacritice.", " Propozitia a doua, tot fara diacritice. ");
		test("Aceasta este o propoziție cu diacritice. ");
		test("Aceasta este o propoziție cu diacritice.", " Propoziția a doua, cu diacritice. ");

		test("O propoziție!", " Și încă o propoziție. ");
		test("O propoziție...", "Și încă o propoziție. ");
		test("La adresa http://www.archeus.ro găsiți resurse lingvistice. ");
		test("Data de 10.02.2009 nu trebuie să fie separator de propoziții. ");
		test("Astăzi suntem în data de 07.05.2007. ");
		test("Astăzi suntem în data de 07/05/2007. ");
		test("La anumărul (1) avem puține informații. ");
		test("To jest 1. wydanie.");
		test("La anumărul 1. avem puține informații. ");
		test("La anumărul 13. avem puține informații. ");
		test("La anumărul 1.3.3 avem puține informații. ");

		test("O singură propoziție... ");
		test("Colegii mei s-au dus... ");
		test("O singură propoziție!!! ");
		test("O singură propoziție??? ");

		test("Propoziții: una și alta. ");

		test("Domnu' a plecat. ");
		test("Profu' de istorie tre' să predea lecția. ");
		test("Sal'tare! ");
		test("'Neaţa! ");
		test("Deodat'apare un urs. ");
		// accente
		test("A făcut două cópii. ");
		test("Ionel adúnă acum ceea ce Maria aduná înainte să vin eu. ");

		// incomplete sentences, need to work for on-thy-fly checking of texts:
		test("Domnu' a plecat");
		test("Domnu' a plecat.", " El nu a plecat");

		test("Se pot întâlni și abrevieri precum S.U.A. " + "sau B.C.R. într-o singură propoziție.");
		test("Se pot întâlni și abrevieri precum S.U.A. sau B.C.R.", " Aici sunt două propoziții.");
		test("Același lucru aici...", "Aici sunt două propoziții.");
		test("Același lucru aici... dar cu o singură propoziție.");

		test("„O propoziție!”", " O alta.");
		test("„O propoziție!!!”", " O alta.");
		test("„O propoziție?”", " O alta.");
		test("„O propoziție?!?”", " O alta.");
		test("«O propoziție!»", " O alta.");
		test("«O propoziție!!!»", " O alta.");
		test("«O propoziție?»", " O alta.");
		test("«O propoziție???»", " O alta.");
		test("«O propoziție?!?»", " O alta.");
		test("O primă propoziție.", " (O alta.)");

		test("A venit domnu' Vasile. ");
		test("A venit domnu' acela. ");

		// one/two returns = paragraph = new sentence:
		SrxSplitCompare.compare(new String[] { "A venit domnul", "\n\n", "Vasile." }, segmenter);
		SrxSplitCompare.compare(new String[] { "A venit domnul", "\n", "Vasile." }, segmenter);
		SrxSplitCompare.compare(new String[] { "A venit domnu'", "\n\n", "Vasile." }, segmenter);
		SrxSplitCompare.compare(new String[] { "A venit domnu'", "\n", "Vasile." }, segmenter);
		// Missing space after sentence end:
		test("El este din România!", "Acum e plecat cu afaceri.");

		test("Temperatura este de 30°C.", " Este destul de cald.");
		test("A alergat 50 m.", " Deja a obosit.");

		// From the abbreviation list:
		test("Pentru dvs. vom face o excepție.");
		test("Pt. dumneavoastră vom face o excepție.");
		test("Pt. dvs. vom face o excepție.");
		// din punct de vedere
		test("A expus problema d.p.d.v. artistic.");
		test("A expus problema dpdv. artistic.");
		// şi aşa mai departe.
		test("Are mere, pere, șamd. dar nu are alune.");
		test("Are mere, pere, ș.a.m.d. dar nu are alune.");
		test("Are mere, pere, ș.a.m.d.", " În schimb, nu are alune.");
		// şi celelalte
		test("Are mere, pere, ş.c.l. dar nu are alune.");
		test("Are mere, pere, ş.c.l.", " Nu are alune.");
		// etc. et cetera
		test("Are mere, pere, etc. dar nu are alune.");
		test("Are mere, pere, etc.", " Nu are alune.");
		// ş.a. - şi altele
		test("Are mere, pere, ș.a. dar nu are alune.");

		// pag, leg, art
		test("Lecția începe la pag. următoare și are trei pagini.");
		test("Lecția începe la pag. 20 și are trei pagini.");
		test("A acționat în conformitate cu lg. 144, art. 33.");
		test("A acționat în conformitate cu leg. 144, art. 33.");
		test("A acționat în conformitate cu legea nr. 11.");
		test("Lupta a avut loc în anul 2000 î.H. și a durat trei ani.");

		// lunile anului, abreviate
		test("Discuția a avut loc pe data de douăzeci aug. și a durat două ore.");
		test("Discuția a avut loc pe data de douăzeci ian. și a durat două ore.");
		test("Discuția a avut loc pe data de douăzeci feb. și a durat două ore.");
		test("Discuția a avut loc pe data de douăzeci ian.", " A durat două ore.");

		// M.Ap.N. - Ministerul Apărării Nationale
		// there are 2 rules for this in segment.srx. Can this be done with only
		// one rule?
		test("A fost și la M.Ap.N. dar nu l-au primit. ");
		test("A fost și la M.Ap.N.", " Nu l-au primit. ");

		// sic!
		test("Apo' da' tulai (sic!) că mult mai e de mers.");
		test("Apo' da' tulai(sic!) că mult mai e de mers.");

		// […]
		//testSplit("Aici este o frază […] mult prescurtată.");
		//testSplit("Aici este o frază [...] mult prescurtată.");
	}

	private void test(final String... sentences) {
		SrxSplitCompare.compare(sentences, segmenter);
	}
}
