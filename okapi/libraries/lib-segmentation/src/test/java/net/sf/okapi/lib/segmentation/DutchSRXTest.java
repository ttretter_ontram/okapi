/*===========================================================================
  Copyright (C) 2023 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/
package net.sf.okapi.lib.segmentation;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class DutchSRXTest {
	private static OkapiSegmenter segmenter;

	@BeforeClass
	public static void init() {
		segmenter = new OkapiSegmenter("nl");
	}

	@Test
	public void testOkapiSegmentTest() {
		segment();
	}

	private void segment() {
		// NOTE: sentences here need to end with a space character so they
		// have correct whitespace when appended:
		test("Dit is een zin.");
		test("Dit is een zin.", " Nog een.");
		test("Een zin!", " Nog een.");
		test("Een zin... ", "Nog een.");
		test("Op http://www.test.de vind je een website.");
		test("De brief is op 3.10 gedateerd.");
		test("De brief is op 31.1 gedateerd.");
		test("De breif is op 3.10.2000 gedateerd.");

		test("Vandaag is het 13.12.2004.");
		test("Op 24.09 begint het.");
		test("Om 17:00 begint het.");
		test("In paragraaf 3.9.1 is dat beschreven.");

		test("Januari jl. is dat vastgelegd.");
		test("Appel en pruimen enz. werden gekocht.");
		test("De afkorting n.v.t. betekent niet van toepassing.");

		test("Bla et al. blah blah.");

		test("Dat is,, of het is bla.");
		test("Dat is het..", " Zo gaat het verder.");

		test("Dit hier is een(!) zin.");
		test("Dit hier is een(!!) zin.");
		test("Dit hier is een(?) zin.");
		test("Dit hier is een(???) zin.");
		test("Dit hier is een(???) zin.");

		test("»De papagaai is groen.«", " Dat was hij al.");
		test("»De papagaai is groen«, zei hij.");

		test("Als voetballer wordt hij nooit een prof.", " Maar prof. N.A.W. Th.Ch. Janssen wordt dat wel.");

		// TODO, zin na dubbele punt
		test("Dat was het: helemaal niets.");
		test("Dat was het: het is een nieuwe zin.");
	}

	private void test(String... sentences) {
		SrxSplitCompare.compare(sentences, segmenter);
	}

}
