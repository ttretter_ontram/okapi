/*===========================================================================
  Copyright (C) 2023 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/
package net.sf.okapi.lib.segmentation;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ItalianSRXTest {
	private static OkapiSegmenter segmenter;

	@BeforeClass
	public static void init() {
		segmenter = new OkapiSegmenter("it");
	}

	@Test
	public void testOkapiSegmentTest() {
		segment();
	}

	private void segment() {
		test("Il Castello Reale di Racconigi è situato a Racconigi, in provincia di Cuneo ma poco distante da Torino.",
				" Nel corso della sua quasi millenaria storia ha visto numerosi rimaneggiamenti e divenne di proprietà dei Savoia a partire dalla seconda metà del XIV secolo.");
		test("Dott. Bunsen Honeydew"); // abbreviation
		test("Salve Sig.ra Mengoni!", " Come sta oggi?");
		test("Una lettera si può iniziare in questo modo «Il/la sottoscritto/a.».");
		test("La casa costa 170.500.000,00€!");
		test("Salve Sig.ra Mengoni!", " Come sta oggi?");
		test("Buongiorno!", " Sono l'Ing. Mengozzi.", " È presente l'Avv. Cassioni?");
		test("Mi fissi un appuntamento per mar. 23 Nov..", "Grazie.");
		test("Ecco il mio tel.:01234567.", " Mi saluti la Sig.na Manelli.", " Arrivederci.");
		test("La centrale meteor. si è guastata.", " Gli idraul. son dovuti andare a sistemarla.");
		test("Hanno creato un algoritmo allo st. d. arte.", " Si ringrazia lo psicol. Serenti.");
		//testSplit("Chiamate il V.Cte. delle F.P., adesso!");
		test("Giancarlo ha sostenuto l'esame di econ. az..");
		test("Egregio Dir. Amm., le faccio sapere che l'ascensore non funziona.");
		test("Ricordatevi che dom 25 Set. sarà il compleanno di Maria; dovremo darle un regalo.");
		test("La politica è quella della austerità; quindi verranno fatti tagli agli sprechi.");
		test("Nel tribunale, l'Avv. Fabrizi ha urlato \"Io, l'illustrissimo Fabrizi, vi si oppone!\".");
		test("1°C corrisponde a 33.8°F.");
		test("La casa costa 170.500.000,00€!");
		test("Devi comprare : 1)pesce 2)sale.");
	}

	private void test(String... sentences) {
		SrxSplitCompare.compare(sentences, segmenter);
	}

}
