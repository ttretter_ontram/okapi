/*===========================================================================
  Copyright (C) 2023 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/
package net.sf.okapi.lib.segmentation;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.ISegmenter;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Segment;
import net.sf.okapi.common.resource.TextUnit;

import java.util.LinkedList;
import java.util.List;

public class OkapiSegmenter {
	private ISegmenter segmenter;
	
	public OkapiSegmenter(final String srxLanguageName) {
		SRXDocument srxDoc = new SRXDocument();
		srxDoc.loadRules(SRXSegmenter.class.getClassLoader().getResourceAsStream("net/sf/okapi/lib/segmentation/defaultSegmentation.srx"));
		srxDoc.setTrimLeadingWhitespaces(false);
		srxDoc.setTrimTrailingWhitespaces(false);
		segmenter = srxDoc.compileLanguageRules(new LocaleId(srxLanguageName), null);
	}

	public List<String> tokenize(String text) {
		List<String> sl = new LinkedList<>();
		ITextUnit tu = new TextUnit("temp", text);
		tu.createSourceSegmentation(segmenter);
		
		for (Segment s : tu.getSource().getSegments()) {
			sl.add(s.text.getText());
		}
		return sl;
	}
}
