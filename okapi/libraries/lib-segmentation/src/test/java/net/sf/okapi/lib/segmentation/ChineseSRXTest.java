/*===========================================================================
  Copyright (C) 2023 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/
package net.sf.okapi.lib.segmentation;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ChineseSRXTest {
	private static OkapiSegmenter segmenter;

	@BeforeClass
	public static void init() {
		segmenter = new OkapiSegmenter("zh");
	}

	@Test
	public void testOkapiSegmentTest() {
		segment2();
	}

	public void segment2() {
		test("Linux是一種自由和開放源碼的類UNIX操作系統。", "该操作系统的内核由林纳斯·托瓦兹在1991年10月5日首次发布。", "在加上使用者空間的應用程式之後，");
		test("我们明天一起去看《摔跤吧！", "爸爸》好吗？", "好！");
	}

	private void test(String... sentences) {
		SrxSplitCompare.compare(sentences, segmenter);
	}
}
