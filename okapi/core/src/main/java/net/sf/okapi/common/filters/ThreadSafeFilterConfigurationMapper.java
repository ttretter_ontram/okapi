package net.sf.okapi.common.filters;

import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.IParametersEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * <b>Will replace {@link FilterConfigurationMapper} in the future as the default implementation</b>
 * Implementation of IFilterConfigurationMapper, without some of the eccentricities of
 * {@link FilterConfigurationMapper}.
 * In particular,this one is thread safe by allowing thread local custom configs to be registered.
 * Should be used in most server-based implementations.
 */
public class ThreadSafeFilterConfigurationMapper implements IFilterConfigurationMapper {
    private static final Logger logger = LoggerFactory.getLogger(ThreadSafeFilterConfigurationMapper.class);

    /**
     * Each time a thread starts up, initialize it with a static set of default configs.
     * These are persistent and are reused in subsequent calls to the same thread.
     */
    private final ThreadLocal<Map<String, FilterConfiguration>> configMap;

    /**
     * Create an empty {@link IFilterConfigurationMapper}
     */
    public ThreadSafeFilterConfigurationMapper() {
        this.configMap = ThreadLocal.withInitial(ThreadSafeFilterConfigurationMapper::loadConfigs);
    }

    /**
     * Create {@link IFilterConfigurationMapper} using the {@link Supplier}
     * Example:
     * <code><pre>
     * private Map<String, FilterConfiguration> myConfigLoad() {
     * // this is my implementation loading the filters it needs
     * }
     *
     * // client code
     * IFilterConfigurationMapper fcMapper =
     * new ThreadSafeFilterConfigurationMapper(() -> MyClass::myConfigLoad);
     * </code></pre>
     *
     * @param fcSupplier user implemented {@link FilterConfiguration} map.
     */
    public ThreadSafeFilterConfigurationMapper(Supplier<Map<String, FilterConfiguration>> fcSupplier) {
        this.configMap = ThreadLocal.withInitial(fcSupplier);
    }

    private static Map<String, FilterConfiguration> loadConfigs() {
        // For now, we depend on outside callers to populate the configMap
        return new ConcurrentSkipListMap<>();
    }

    private static IParameters createDefaultParameters(FilterConfiguration fc) {

        // take the hit and create another filter instance so we get a unique IParameters instance
        try (IFilter filter = instantiateFilter(fc.filterClass)) {
            IParameters params = filter.getParameters();
            // use the parameters previous loaded
             if (fc.parameters != null) {
                // load parameters from config as these may have been updated externally
                params.fromString(fc.parameters.toString());
                return params;
            } else if (fc.parametersLocation != null) {
                 params.load(filter.getClass().getResourceAsStream(fc.parametersLocation), false);
                 return params;
            }

            // default parameters
            params.reset();
            return params;
        }
    }

    private static IFilter instantiateFilter(String filterClass) {
        try {
            return (IFilter) Thread.currentThread().getContextClassLoader().loadClass(filterClass).getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException | ClassNotFoundException e) {
            throw new IllegalArgumentException("Could not instantiate: " + filterClass, e);
        }
    }

    @Override
    public void addCustomConfiguration(String configId, IParameters parameters) {
        if (configMap.get().containsKey(configId)) {
            return;
        }

        FilterConfiguration fc;
        fc = new FilterConfiguration();
        fc.custom = true;
        fc.configId = configId;

        // Get the filter
        String[] res = IFilterConfigurationMapper.splitFilterFromConfiguration(fc.configId);

        // Create the filter (this assumes the base-name is the default config ID)
        try (IFilter filter = createFilter(res[0])) {
            // Set the data
            fc.filterClass = filter.getClass().getName();
            fc.mimeType = filter.getMimeType();
            fc.description = "Configuration " + fc.configId;
            fc.name = fc.configId;
            fc.parameters = filter.getParameters();
            fc.parameters.fromString(parameters.toString());
            configMap.get().putIfAbsent(fc.configId, fc);
            logger.debug("Added custom config: {} with parameters: {}", configId, parameters);
        }
    }

    @Override
    public void addCustomConfiguration(String configId, String parameters) {
        if (configMap.get().containsKey(configId)) {
            return;
        }

        FilterConfiguration fc;
        fc = new FilterConfiguration();
        fc.custom = true;
        fc.configId = configId;

        // Get the filter
        String[] res = IFilterConfigurationMapper.splitFilterFromConfiguration(fc.configId);

        // Create the filter (this assumes the base-name is the default config ID)
        try (IFilter filter = createFilter(res[0])) {
            // Set the data
            fc.filterClass = filter.getClass().getName();
            fc.mimeType = filter.getMimeType();
            fc.description = "Configuration " + fc.configId;
            fc.name = fc.configId;
            fc.parameters = filter.getParameters();
            fc.parameters.fromString(parameters);
            configMap.get().putIfAbsent(fc.configId, fc);
            logger.debug("Added custom config: {} with parameters: {}", configId, parameters);
        }
    }

    @Override
    public void addConfigurations(String filterClass) {
        // Add the filter to the list
        try (IFilter filter = instantiateFilter(filterClass)) {
            // Get the available configurations for this filter
            List<FilterConfiguration> list = filter.getConfigurations();
            if ((list == null) || (list.isEmpty())) {
                logger.warn("No configuration provided for '{}'", filterClass);
                return;
            }

            for (FilterConfiguration config : list) {
                // skip if we already have this configuration
                if (configMap.get().containsKey(config.configId)) {
                    continue;
                }

                // Load the default parameters as we may need to update them later, and we don't want null values
                config.parameters = createDefaultParameters(config);

                if (config.filterClass == null) {
                    logger.warn("Configuration without filter class name in '{}'", config);
                    config.filterClass = filterClass;
                }
                if (config.name == null) {
                    logger.warn("Configuration without name in '{}'", config);
                    config.name = config.toString();
                }
                if (config.description == null) {
                    logger.warn("Configuration without description in '{}'", config);
                    config.description = config.toString();
                }
                configMap.get().putIfAbsent(config.configId, config);
                logger.debug("Added custom config: {} with parameters: {}", config.configId, config.parameters);
            }
        }
    }

    @Override
    public void addConfiguration(FilterConfiguration config) {
        configMap.get().putIfAbsent(config.configId, config);
        logger.debug("Added custom config: {} with parameters: {}", config.configId, config.parameters);
    }

    /**
     * Returns the {@link IFilter} for the given configId.
     * @param configId the filter configuration identifier to use for look-up.
     * @param existingFilter currently ignored and is always null
     * @deprecated use {@link #createFilter(String)} instead
     * @return the filter instance
     */
    @Deprecated
    @Override
    public IFilter createFilter(String configId, IFilter existingFilter) {
        // Get the configuration object for the given configId
        FilterConfiguration fc = getConfiguration(configId);
        if (fc == null) {
            logger.error("Cannot find filter configuration '{}'", configId);
            return null;
        }

        // Instantiate the filter - we force recreation since reuse is dangerous
        try (IFilter filter = instantiateFilter(fc.filterClass)) {
            filter.setFilterConfigurationMapper(this);
            // make a copy of the parameters from the configs
            IParameters params = filter.getParameters();
            if (params != null) {
                params.fromString(fc.parameters.toString());
                filter.setParameters(params);
            }

            return filter;
        }
    }

    @Override
    public void clearConfigurations(boolean customOnly) {
        if (customOnly) {
            configMap.get().values().removeIf(fc -> fc.custom);
        } else {
            configMap.get().clear();
        }
    }

    @Override
    public IFilter createFilter(String configId) {
        return createFilter(configId, null);
    }

    @Override
    public FilterConfiguration getConfiguration(String configId) {
        // Load baseline configs if they haven't been previously loaded in this thread
        Map<String, FilterConfiguration> cm = configMap.get();
        if (cm == null) {
            throw new IllegalStateException("No filter configurations have been loaded loaded");
        }
        return cm.get(configId);
    }

    @Override
    public FilterConfiguration getDefaultConfiguration(String mimeType) {
        // xliff1.2 and xliff2 use the same mimetype use the autoxliff
        // filter to disambiguate.
        FilterConfiguration autoXliff = getConfiguration("okf_autoxliff");
        if (autoXliff != null && autoXliff.mimeType.equals(mimeType)) {
            return autoXliff;
        }

        FilterConfiguration defaultConfig = null;
        for (FilterConfiguration config : configMap.get().values()) {
            if (config.mimeType == null || !config.mimeType.equals(mimeType)) {
                continue;
            }

            // Assume that no parameters means it's the default
            if (config.parametersLocation == null) {
                return config;
            }

            // The first registered match is default if all have `parametersLocation`
            if (defaultConfig == null) {
                defaultConfig = config;
            }
        }

        return defaultConfig;
    }

    @Override
    public FilterConfiguration getDefaultConfigurationFromExtension(String ext) {
        String tmp = ext.toLowerCase() + ";";
        if (tmp.charAt(0) != '.') {
            tmp = ".".concat(tmp);
        }

        // xliff1.2 and xliff2 use the same file extensions use the autoxliff
        // filter to disambiguate.
        FilterConfiguration autoXliff = getConfiguration("okf_autoxliff");
        if (autoXliff != null && autoXliff.extensions.contains(tmp)) {
            return autoXliff;
        }

        for (FilterConfiguration config : configMap.get().values()) {
            if (config.extensions != null) {
                // Just in case some plugin forgot the trailing semicolon
                String configExtensions = config.extensions + ";";
                if (configExtensions.contains(tmp)) {
                    return config;
                }
            }
        }
        return null;
    }

    @Override
    public IParameters getParameters(FilterConfiguration config) {
        return config.parameters;
    }

    @Override
    public IParameters getParameters(FilterConfiguration config, IFilter existingFilter) {
        IFilter filter = createFilter(config.configId, existingFilter);
        return filter.getParameters();
    }

    @Override
    public FilterConfiguration createCustomConfiguration(FilterConfiguration baseConfig) {
        // This one we do a different way
        throw new UnsupportedOperationException();
    }

    @Override
    public IParameters getCustomParameters(FilterConfiguration config, IFilter existingFilter) {
        throw new UnsupportedOperationException();
    }

    @Override
    public IParameters getCustomParameters(FilterConfiguration config) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterator<FilterConfiguration> getAllConfigurations() {
        return configMap.get().values().iterator();
    }

    @Override
    public List<FilterConfiguration> getFilterConfigurations(String filterClass) {
        ArrayList<FilterConfiguration> list = new ArrayList<>();
        for (FilterConfiguration config : configMap.get().values()) {
            if (config.filterClass != null) {
                if (config.filterClass.equals(filterClass)) {
                    list.add(config);
                }
            }
        }
        return list;
    }

    @Override
    public IParametersEditor createConfigurationEditor(String configId, IFilter existingFilter) {
        throw new UnsupportedOperationException();
    }

    @Override
    public IParametersEditor createConfigurationEditor(String configId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<FilterInfo> getFiltersInfo() {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<FilterConfiguration> getMimeConfigurations(String mimeType) {
        ArrayList<FilterConfiguration> list = new ArrayList<>();
        for (FilterConfiguration config : configMap.get().values()) {
            if (config.mimeType != null) {
                if (config.mimeType.equals(mimeType)) {
                    list.add(config);
                }
            }
        }
        return list;
    }

    @Override
    public void removeConfiguration(String configId) {
        configMap.get().remove(configId);
    }

    @Override
    public Iterator<FilterConfiguration> iterator() {
        return configMap.get().values().iterator();
    }

    @Override
    public void forEach(Consumer<? super FilterConfiguration> action) {
        configMap.get().values().forEach(action);
    }

    @Override
    public Spliterator<FilterConfiguration> spliterator() {
        return configMap.get().values().spliterator();
    }

    @Override
    public void removeConfigurations(String filterClass) {
        Map.Entry<String, FilterConfiguration> entry;
        Iterator<Map.Entry<String, FilterConfiguration>> iter = configMap.get().entrySet().iterator();
        while (iter.hasNext()) {
            entry = iter.next();
            if (entry.getValue().filterClass.equals(filterClass)) {
                iter.remove();
            }
        }


    }

    @Override
    public void saveCustomParameters(FilterConfiguration config, IParameters params) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteCustomParameters(FilterConfiguration config) {
        throw new UnsupportedOperationException();
    }

    /**
     * Load one or more filter classes and prepare their FilterConfiguration objects. This code lives
     * here because of overlap with filter instantiation code (it needs to instantiate short-lived
     * instances in order to query them for their pre-packaged configurations).
     *
     * <p>The result of building is a map of configId (for example, "okf_html@foo" or "okf_openxml")
     * to FilterConfiguration objects.
     */
    public static class ConfigBuilder {
        private final Map<String, FilterConfiguration> configs = new HashMap<>();

        public ConfigBuilder addConfigurations(Class<? extends IFilter> filterClass) {
            try {
                try (IFilter filter = instantiateFilter(filterClass.getCanonicalName())) {
                    for (FilterConfiguration config : filter.getConfigurations()) {
                        if (configs.containsKey(config.configId)) {
                            continue;
                        }
                        // Load the default parameters as we may need to update them later, and we don't want null values
                        config.parameters = createDefaultParameters(config);
                        configs.put(config.configId, config);
                    }
                }
                return this;
            } catch (Exception e) {
                throw new IllegalArgumentException("Couldn't add configurations for " + filterClass.getName(), e);
            }
        }

        public ConfigBuilder updateConfiguration(String configId, String params) {
            try {
                String[] parts = IFilterConfigurationMapper.splitFilterFromConfiguration(configId);
                // Look up the base filter
                FilterConfiguration baseFilter = configs.get(parts[0]);
                if (baseFilter == null) {
                    throw new IllegalArgumentException("Couldn't find base filter for " + configId);
                }
                FilterConfiguration fc = configs.get(configId);
                if (fc == null) {
                    throw new IllegalArgumentException("Couldn't find filter configuration for " + configId);
                }
                // Update the parameters
                fc.parameters.fromString(params);
                configs.put(configId, fc);
                return this;
            } catch (Exception e) {
                throw new IllegalArgumentException("Couldn't update configurations for " + configId);
            }
        }

        public ConfigBuilder updateConfigurations(Function<IParameters, IParameters> function) {
            for (FilterConfiguration config : configs.values()) {
                config.parameters = function.apply(config.parameters);
            }
            return this;
        }

        public Map<String, FilterConfiguration> build() {
            return Collections.synchronizedMap(configs);
        }
    }
}
