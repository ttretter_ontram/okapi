/*===========================================================================
  Copyright (C) 2008-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.common.resource;

import net.sf.okapi.common.filterwriter.TMXFilterWriter;
import net.sf.okapi.common.filterwriter.TMXWriter;

import java.util.EnumSet;
import java.util.Objects;

/**
 * Represents a read-only or a modifiable property associated with a resource.
 * For example the HREF attribute of the element A in HTML would be a property.
 * Note that translatable data (such as the text of an attribute ALT of an IMG element in HTML)
 * must be stored in {@link TextUnit} rather that Property.
 */
public class Property implements Cloneable {

    public static final String ENCODING = "encoding";
    public static final String LANGUAGE = "language";
    public static final String APPROVED = "approved";

    public static final String COORDINATES = "coordinates";
    public static final String STATE_QUALIFIER = "state-qualifier";
    public static final String STATE = "state";

    /**
     * Helper property used only for placing back some ITS and other annotations.
     */
    public static final String ITS_LQI = "locQualityIssuesRef";
    public static final String ITS_PROV = "provenanceRecordsRef";
    public static final String ITS_MTCONFIDENCE = "mtConfidence";
    public static final String XLIFF_TOOL = "tool";
    public static final String XLIFF_PHASE = "phase";

    /**
     * Sizing properties, can be applied to StartGroup or ITextUnit objects.
     */
    public static final String MAX_WIDTH = "maxwidth";
    public static final String MAX_HEIGHT = "maxheight";
    public static final String SIZE_UNIT = "size-unit";

    /**
     * For TMX filter - may be used by {@link TMXWriter} or {@link TMXFilterWriter}
     */
    public static final String TMX_i = "TMX_i";
    public static String TMX_hi = "TMX_hi";
    public static String TMX_x = "TMX_x";

    public static EnumSet<Type> FILTER_AND_DISPLAY = EnumSet.of(Type.FILTER_ONLY,Property.Type.DISPLAY);
    public static EnumSet<Type> DISPLAY_ONLY = EnumSet.of(Property.Type.DISPLAY);

    private String name;
    private String value;
    private boolean isReadOnly;
    private EnumSet<Type> types;

    /**
     * Creates a new property object with a name, a vale and its read-only flag.
     *
     * @param name       the name of the property (case-sensitive).
     * @param value      the value of the property.
     * @param isReadOnly true if the property cannot be modified using the filter, false if you
     *                   can modify the value in the output document.
     */
    public Property(String name, String value, boolean isReadOnly) {
        this.name = name;
        this.value = value;
        this.isReadOnly = isReadOnly;
        this.types = EnumSet.of(Type.FILTER_ONLY);
    }

    public Property(String name, String value, boolean isReadOnly, EnumSet<Type> type) {
        this(name, value, isReadOnly);
        this.types = type;
    }

    /**
     * Creates a new read-only property object with a name and a value.
     *
     * @param name  the name of the property (case-sensitive)
     * @param value the value of the property.
     */
    public Property(String name, String value) {
        this(name, value, true);
    }

    public Property(String name, String value, EnumSet<Type> type) {
        this(name, value, true);
        this.types = type;
    }

    /**
     * Gets the string representation of this property. This is the same as its value.
     *
     * @return the value of the property.
     */
    @Override
    public String toString() {
        return value;
    }

    /**
     * Clones this property.
     *
     * @return a new property object that is a copy of this one.
     */
    @Override
    public Property clone() {
        return new Property(name, value, isReadOnly);
    }

    /**
     * Gets the name of this property.
     *
     * @return the name of this property.
     */
    public String getName() {
        return name;
    }

    // For serialization only
    protected void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the value of this property.
     *
     * @return the value of this property.
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets a new value for this property.
     *
     * @param value the new value to set.
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the boolean value of this property. Use this helper method to get a boolean from the
     * value of this property. The values "true" and "yes" (in any case) returns true, any other
     * value returns false. No verification is done to see if the value is really boolean or not.
     *
     * @return true is the property value is "true", "yes" (case-insensitive), false otherwise.
     */
    public boolean getBoolean() {
        if (value == null) return false;
        return (value.equalsIgnoreCase("yes") || value.equalsIgnoreCase("true"));
    }

    /**
     * Indicates if this property is read-only.
     * <p>All property can be changed, but the ones flagged as read-only
     * cannot be modified when re-writing the document from where they have been extracted.
     *
     * @return true if the property cannot be modified in the filter output, false if it can be modified.
     */
    public boolean isReadOnly() {
        return isReadOnly;
    }

    protected void setReadOnly(boolean isReadOnly) {
        this.isReadOnly = isReadOnly;
    }

    /**
     * Gets the types of the Property. @see {@link Type}
     *
     * @return The types of the Property.
     */
    public EnumSet<Type> getTypes() {
        return types;
    }

    /**
     * Set the types of the Property. @see {@link Type}
     *
     * @param types The types to set.
     */
    public void setTypes(EnumSet<Type> types) {
        this.types = types;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Property property = (Property) o;
        return isReadOnly() == property.isReadOnly() && Objects.equals(getName(), property.getName()) && Objects.equals(getValue(), property.getValue()) && Objects.equals(getTypes(), property.getTypes());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getValue(), isReadOnly(), getTypes());
    }

    /**
	 * Property types.
	 */
    public enum Type {
		/**
		 * Only used by a {@link net.sf.okapi.common.filters.IFilter} or {@link net.sf.okapi.common.filterwriter.IFilterWriter}.
		 * Not meant to be displayed to end users for translation.
		 * */
        FILTER_ONLY("filter-only"),
		/**
		 * Meant to be displayed to end users for translation.
		 */
        DISPLAY("display"),
		/**
		 * ITS (Internationalization Tag Set) specific properties
		 */
        ITS("its"),

		/**
		 * Used by {@link net.sf.okapi.common.pipeline.IPipelineStep}.
		 */
		PIPELINE("pipeline");

        private final String name;

        Type(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
