package net.sf.okapi.common.resource;

import net.sf.okapi.common.Util;

import java.util.Comparator;

public class CodeComparatorOnType implements Comparator<Code> {

    @Override
    public int compare(Code c1, Code c2) {
        if (c1 == c2) {
            return 0;
        }

        if (c1 == null || c2 == null) {
            return -1;
        }

        // if Code.type is empty or null we have no information to match
        // we don't want to match empty with empty or null to null.
        if (Util.isEmpty(c1.getType()) && Util.isEmpty(c2.getType())) {
            return -1;
        }

        String c = c1.getType() == null ? "" : c1.getType();
        return c.compareTo(c2.getType());
    }
}