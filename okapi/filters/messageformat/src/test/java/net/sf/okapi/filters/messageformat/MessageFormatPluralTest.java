/*
 * ====================================================================
 *   Copyright (C) $time.year by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =====================================================================
 */

package net.sf.okapi.filters.messageformat;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.StringUtil;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class MessageFormatPluralTest {
    @Test
    public void testWithPluralMessage2() throws Exception {
        String message = "You have {itemCount, plural, =0 {no items} one {1 item} other {{itemCount} items}}";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            p.addPluralForms(LocaleId.ENGLISH, LocaleId.ARABIC);
            assertEquals( "You have {itemCount, plural, =0 {no items} one {1 item} other {{itemCount} items} zero {{itemCount} items} two {{itemCount} items} few {{itemCount} items} many {{itemCount} items}}", StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testWithSelectMessage() throws Exception {
        String message = "{gender, select, male {He} female {She} other {They}} is my friend.";
        try (MessageFormatParser p = new MessageFormatParser()) {
            p.parse(message);
            p.addPluralForms(LocaleId.ENGLISH, LocaleId.RUSSIAN);
            assertEquals(message, StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testAddPluralForms_Cardinal() throws Exception {
        try (MessageFormatParser p = new MessageFormatParser("I have {num_apples, plural, one {an apple} other {# apples}}")) {
            p.addPluralForms(LocaleId.ENGLISH, LocaleId.RUSSIAN);
            assertEquals("I have {num_apples, plural, one {an apple} other {# apples} few {# apples} many {# apples}}", StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testAddMultiplePluralForms() throws Exception {
        try (MessageFormatParser p = new MessageFormatParser("{0, plural, one {You have {1, plural, one {# apple} other {# apples}}} other {# apples}}")) {
            p.addPluralForms(LocaleId.ENGLISH, LocaleId.RUSSIAN);
            assertEquals("{0, plural, one {You have {1, plural, one {# apple} other {# apples} few {# apples} many {# apples}}} other {# apples} few {# apples} many {# apples}}",
                    StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testAddPluralForms_Ordinal() throws Exception {
        try (MessageFormatParser p = new MessageFormatParser("You are visitor number {num_visitor, selectordinal, one {#st} two {#nd} few {#rd} other {#th}}")) {
            p.addPluralForms(LocaleId.ENGLISH, LocaleId.RUSSIAN);
            assertEquals("You are visitor number {num_visitor, selectordinal, other {#th}}", StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testAddPluralForms_OrdinalAndPlural() throws Exception {
        try (MessageFormatParser p = new MessageFormatParser("You have {num_guests, plural,\n" +
                "  =0 {no guests}\n" +
                "  one {one guest}\n" +
                "  other {# guests}\n" +
                "} in your party, and you are the {num_visitor, selectordinal,\n" +
                "  one {#st}\n" +
                "  two {#nd}\n" +
                "  few {#rd}\n" +
                "  other {#th}\n" +
                "} visitor.")) {

            p.addPluralForms(LocaleId.ENGLISH, LocaleId.RUSSIAN);
            assertEquals("You have {num_guests, plural, =0 {no guests} one {one guest} other {# guests} few {# guests} many {# guests}} in your party, and you are the {num_visitor, selectordinal, other {#th}} visitor.", StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testAddPluralForms_Ordinal2() throws Exception {
        try (MessageFormatParser p = new MessageFormatParser("{place, selectordinal, one {#st place} two {#nd place} few {#rd place} other {#th place}}")) {
            p.addPluralForms(LocaleId.ENGLISH, LocaleId.fromString("ro"));
            assertEquals("{place, selectordinal, one {#st place} other {#th place}}", StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testAddPluralForms_Ordinal3() throws Exception {
        try (MessageFormatParser p = new MessageFormatParser("{count, selectordinal,\n" +
                "  =0 {No books}\n" +
                "  one {One book}\n" +
                "  two {{count} books} \n" +
                "  few {{count} books}\n" +
                "  other {{count} books}\n" +
                "  selectordinal0 {{count}st book}\n" +
                "  selectordinal1 {{count}nd book}\n" +
                "  selectordinal2 {{count}rd book}\n" +
                "  selectordinal3 {{count}th book}\n" +
                "}")) {
            p.addPluralForms(LocaleId.ENGLISH, LocaleId.fromString("lv"));
            assertEquals("{count, selectordinal, =0 {No books} other {{count} books} selectordinal0 {{count}st book} selectordinal1 {{count}nd book} selectordinal2 {{count}rd book} selectordinal3 {{count}th book}}", StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testAddPluralForms_Gender() throws Exception {
        try (MessageFormatParser p = new MessageFormatParser("{gender, select, male {He has # apples} female {She has # apples} other {They have # apples}}")) {
            p.addPluralForms(LocaleId.ENGLISH, LocaleId.fromString("ro"));
            assertEquals("{gender, select, male {He has # apples} female {She has # apples} other {They have # apples}}", StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testAddPluralForms_Mixed() throws Exception {
        try (MessageFormatParser p = new MessageFormatParser("{gender, select, male {{num_apples, plural, one {He has an apple} other {He has # apples}}} female {{num_apples, plural, one {She has an apple} other {She has # apples}}} other {{num_apples, plural, one {They have an apple} other {They have # apples}}}}")) {

            p.addPluralForms(LocaleId.ENGLISH, LocaleId.ARABIC);
            assertEquals("{gender, select, male {{num_apples, plural, one {He has an apple} other {He has # apples} zero {He has # apples} two {He has # apples} few {He has # apples} many {He has # apples}}} female {{num_apples, plural, one {She has an apple} other {She has # apples} zero {She has # apples} two {She has # apples} few {She has # apples} many {She has # apples}}} other {{num_apples, plural, one {They have an apple} other {They have # apples} zero {They have # apples} two {They have # apples} few {They have # apples} many {They have # apples}}}}", StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testAddPluralForms_MixedDeep() throws Exception {
        try (MessageFormatParser p = new MessageFormatParser("{gender, select, male {{num_apples, plural, one {He has {num_oranges, plural, one {an orange} other {# oranges}}} other {He has # apples}}} female {{num_apples, plural, one {She has an apple} other {She has # apples}}} other {{num_apples, plural, one {They have an apple} other {They have # apples}}}}")) {

            p.addPluralForms(LocaleId.ENGLISH, LocaleId.ARABIC);
            assertEquals("{gender, select, male {{num_apples, plural, one {He has {num_oranges, plural, one {an orange} other {# oranges} zero {# oranges} two {# oranges} few {# oranges} many {# oranges}}} other {He has # apples} zero {He has # apples} two {He has # apples} few {He has # apples} many {He has # apples}}} female {{num_apples, plural, one {She has an apple} other {She has # apples} zero {She has # apples} two {She has # apples} few {She has # apples} many {She has # apples}}} other {{num_apples, plural, one {They have an apple} other {They have # apples} zero {They have # apples} two {They have # apples} few {They have # apples} many {They have # apples}}}}", StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testAddPluralForms_MixedNested() throws Exception {
        try (MessageFormatParser p = new MessageFormatParser("{gender, select, male {{num_apples, plural, one {{num_oranges, selectordinal, one {He has an apple and an orange} two {He has an apple and a pair of oranges} few {He has an apple and a few oranges} other {He has an apple and # oranges}}} other {{num_oranges, selectordinal, one {He has # apples and an orange} two {He has # apples and a pair of oranges} few {He has # apples and a few oranges} other {He has # apples and # oranges}}}}} female {{num_apples, plural, one {{num_oranges, selectordinal, one {She has an apple and an orange} two {She has an apple and a pair of oranges} few {She has an apple and a few oranges} other {She has an apple and # oranges}}} other {{num_oranges, selectordinal, one {She has # apples and an orange} two {She has # apples and a pair of oranges} few {She has # apples and a few oranges} other {She has # apples and # oranges}}}}} other {{num_apples, plural, one {{num_oranges, selectordinal, one {They have an apple and an orange} two {They have an apple and a pair of oranges} few {They have an apple and a few oranges} other {They have an apple and # oranges}}} other {{num_oranges, selectordinal, one {They have # apples and an orange} two {They have # apples and a pair of oranges} few {They have # apples and a few oranges} other {They have # apples and # oranges}}}}}}")) {

            p.addPluralForms(LocaleId.ENGLISH, LocaleId.RUSSIAN);
            assertEquals("{gender, select, male {{num_apples, plural, one {{num_oranges, selectordinal, other {He has an apple and # oranges}}} other {{num_oranges, selectordinal, other {He has # apples and # oranges}}} few {{num_oranges, selectordinal, other {He has # apples and # oranges}}} many {{num_oranges, selectordinal, other {He has # apples and # oranges}}}}} female {{num_apples, plural, one {{num_oranges, selectordinal, other {She has an apple and # oranges}}} other {{num_oranges, selectordinal, other {She has # apples and # oranges}}} few {{num_oranges, selectordinal, other {She has # apples and # oranges}}} many {{num_oranges, selectordinal, other {She has # apples and # oranges}}}}} other {{num_apples, plural, one {{num_oranges, selectordinal, other {They have an apple and # oranges}}} other {{num_oranges, selectordinal, other {They have # apples and # oranges}}} few {{num_oranges, selectordinal, other {They have # apples and # oranges}}} many {{num_oranges, selectordinal, other {They have # apples and # oranges}}}}}}", StringUtil.collapseWhitespace(p.toString()));
        }
    }

    @Test
    public void testRemovePluralForms() throws Exception {
        try (MessageFormatParser p = new MessageFormatParser("You have {num_files, plural, one {# file} other {# files} two {# files} few {# files} many {# files}}")) {

            p.addPluralForms(LocaleId.ARABIC, LocaleId.ENGLISH);
            assertEquals("You have {num_files, plural, one {# file} other {# files}}", StringUtil.collapseWhitespace(p.toString()));
        }
    }
}
