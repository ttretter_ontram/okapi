package net.sf.okapi.filters.messageformat;

import com.ibm.icu.text.MessagePattern;
import com.ibm.icu.text.MessagePatternUtil;
import net.sf.okapi.common.encoder.EncoderContext;
import net.sf.okapi.common.encoder.IcuMessageEncoder;
import net.sf.okapi.common.exceptions.OkapiBadFilterInputException;

import java.util.List;

public class PrintVisitor implements INodeVisitor {
    protected final StringBuilder message;
    protected final IcuMessageEncoder encoder;
    protected int indent;

    public PrintVisitor() {
        this.message = new StringBuilder();
        this.encoder = new IcuMessageEncoder();
        this.indent = 0;
    }

    protected String indent() {
        var sb = new StringBuilder();
        for (int i = 0; i < indent; ++i) {
            sb.append("    ");
        }
        return sb.toString();
    }

    @Override
    public void visit(MessagePatternUtil.MessageContentsNode node) {
        switch (node.getType()) {
            case TEXT:
                visit((MessagePatternUtil.TextNode) node);
                break;
            case ARG:
                visit((MessagePatternUtil.ArgNode) node);
                break;
            case REPLACE_NUMBER:
                message.append("#");
                break;

        }
    }

    @Override
    public void visit(MessagePatternUtil.TextNode node) {
        String literalText = node.getText();
        long literalCharCount = literalText.chars().filter(
                (c -> c == '{' || c == '}' || c == '\\')).count();
        if (literalCharCount <= 0) {
            // If there are no literal characters return as-is
            message.append(literalText);
        } else {
            message.append(String.format("%s", encoder.encode(literalText, EncoderContext.TEXT)));
        }
    }

    @Override
    public void visit(MessagePatternUtil.ArgNode node) {
        message.append("{");
        if (node.getArgType() == MessagePattern.ArgType.NONE) {
            message.append(String.format("%s", node.getName()));
        } else {
            message.append(String.format("%s, %s", node.getName(), node.getTypeName()));
            if (node.getSimpleStyle() != null) {
                message.append(", ");
                message.append(node.getSimpleStyle());
            }
        }

        MessagePatternUtil.ComplexArgStyleNode complexNode = node.getComplexStyle();
        if (complexNode != null) {
            visit(complexNode);
        }
        message.append("}");
    }

    @Override
    public void visit(MessagePatternUtil.VariantNode node) {
        message.append(String.format("%s%s ", indent(), node.getSelector()));
        message.append("{");
        visit(node.getMessage());
        message.append("}");
    }

    @Override
    public void visit(MessagePatternUtil.ComplexArgStyleNode node) {
        if (node.getArgType() == MessagePattern.ArgType.CHOICE) {
            // FIXME: deprecated message format should we repair with select or plural?
            throw new OkapiBadFilterInputException("CHOICE format is deprecated and is not supported");
        }

        if (node.hasExplicitOffset()) {
            message.append(String.format(", offset:%d\n", (int) node.getOffset()));
        } else {
            message.append(",\n");
        }

        indent++;
        List<MessagePatternUtil.VariantNode> variants = node.getVariants();
        for (MessagePatternUtil.VariantNode variant : variants) {
            visit(variant);
            message.append("\n");
        }
        message.deleteCharAt(message.length() - 1);
        indent--;
    }

    @Override
    public String toString() {
        return message.toString();
    }
}
