package net.sf.okapi.filters.messageformat;

import com.ibm.icu.text.MessageFormat;
import com.ibm.icu.text.MessagePatternUtil;
import com.ibm.icu.util.Currency;
import net.sf.okapi.common.LocaleId;

import java.time.Duration;
import java.util.*;

public class FormattedVisitor extends PrintVisitor {
    private final LocaleId trgLocale;
    private final HashMap<String, Object> arguments;

    public FormattedVisitor(LocaleId trgLocale) {
        super();
        this.trgLocale = trgLocale;
        this.arguments = new HashMap<>();
    }


    @Override
    public void visit(MessagePatternUtil.ArgNode node) {
        super.visit(node);
        arguments.put(node.getName(), getValue(node));
    }

    private Object getValue(MessagePatternUtil.ArgNode node) {
        switch (node.getArgType()) {
            case SELECT:
                return node.getNumber() > -1 ? node.getNumber() : "female";
            case PLURAL:
            case SELECTORDINAL:
                return 3;
            case NONE:
                return handleSimpleType(node.getName(), node);
            case SIMPLE:
                return handleSimpleType(node.getTypeName(), node);
            default:
                return "Bar";
        }
    }

    private Object handleSimpleType(String type, MessagePatternUtil.ArgNode node) {
        switch (type) {
            case "number":
                return node.getNumber() > -1 ? node.getNumber() : 1.0;
            case "date":
            case "time":
                return createDateObject();
            case "currency":
                return createCurrencyObject();
            case "spellout":
                return 0;
            case "percent":
                return 1.0;
            case "duration":
                Duration duration = Duration.ofMinutes(30);
                return duration.toMinutes();
            default:
                return node.getNumber() > -1 ? node.getNumber() : "Foo";
        }
    }

    private Date createDateObject() {
        long timestamp = 1678271600000L;
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.setTimeInMillis(timestamp);
        return calendar.getTime();
    }

    private Currency createCurrencyObject() {
        return Currency.getInstance(Locale.US);
    }

    @Override
    public String toString() {
        if (arguments.isEmpty()) {
            return message.toString();
        }

        MessageFormat format = new MessageFormat(message.toString(), trgLocale.toIcuLocale());
        return format.format(arguments);
    }
}
