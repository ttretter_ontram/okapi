/*
 * ====================================================================
 *   Copyright (C) $time.year by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =====================================================================
 */

package net.sf.okapi.filters.messageformat;

import net.sf.okapi.common.LocaleId;

import java.util.Set;
import java.util.SortedSet;

/**
 * Holds the result of a {@link PluralRulesUtil#diffPluralRules(LocaleId, LocaleId, com.ibm.icu.text.PluralRules.PluralType)}
 */
public class PluralDiff {
  private final LocaleId srcLocale;
  private final LocaleId trgLocale;
  private final SortedSet<String> added;
  private final SortedSet<String> removed;

  /**
   * Creates a new PluralDiff.
   * @param srcLocale source locale
   * @param trgLocale target locale
   * @param added added plural forms needed in the target
   * @param removed deleted plural forms not found in the target
   */
  public PluralDiff(LocaleId srcLocale, LocaleId trgLocale, SortedSet<String> added, SortedSet<String> removed) {
    this.srcLocale = srcLocale;
    this.trgLocale = trgLocale;
    this.added = added;
    this.removed = removed;
  }

  public LocaleId getSrcLocale() {
    return srcLocale;
  }

  public LocaleId getTrgLocale() {
    return trgLocale;
  }

  public Set<String> getAdded() {
    return added;
  }

  public Set<String> getRemoved() {
    return removed;
  }

  public boolean hasAdded() {
    return !added.isEmpty();
  }

  public boolean hasRemoved() { return !removed.isEmpty(); }
}
