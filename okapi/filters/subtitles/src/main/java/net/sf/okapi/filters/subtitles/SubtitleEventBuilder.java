package net.sf.okapi.filters.subtitles;

import net.sf.okapi.common.IResource;
import net.sf.okapi.common.filters.EventBuilder;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Property;
import net.sf.okapi.filters.vtt.VTTSkeletonPart;

public class SubtitleEventBuilder extends EventBuilder {
    private SubtitleFilter filter;

    public SubtitleEventBuilder(String rootId, IFilter filter) {
        super(rootId, filter);
        setMimeType(filter.getMimeType());
        this.filter = (SubtitleFilter) filter;
    }

    private void addMaxWidth(IResource resource) {
        if (filter.getMaxChar() * filter.getMaxLine() != 0) {
            CaptionAnnotation annotation = resource.getAnnotation(CaptionAnnotation.class);
            int numCaptions = 0;
            if (annotation != null) {
                numCaptions = annotation.getSize();
                annotation.setMaxChar(filter.getMaxChar());
                annotation.setMaxLine(filter.getMaxLine());
            }
            Property maxWidth = new Property(Property.MAX_WIDTH, Integer.toString(filter.getMaxChar() * filter.getMaxLine() * numCaptions), Property.DISPLAY_ONLY);
            resource.setProperty(maxWidth);
            Property sizeUnit = new Property(Property.SIZE_UNIT, "char", Property.DISPLAY_ONLY);
            resource.setProperty(sizeUnit);
        }
    }

    @Override
    protected ITextUnit postProcessTextUnit(ITextUnit textUnit) {
        addMaxWidth(textUnit);
        return textUnit;
    }
}
