/*
 * =============================================================================
 * Copyright (C) 2010-2023 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import java.nio.file.Path;

interface PartPath {
    int numberOfParts();
    PartPath partFor(final int index);
    PartPath subpath(final int beginIndex, final int endIndex);
    PartPath normalized();
    PartPath resolve(final PartPath other);
    PartPath resolve(final String other);
    PartPath relativize(final PartPath other);
    Path asPath();
    String asString();

    class Default implements PartPath {
        private static final String SLASH = "/";
        private static final String BACKSLASH = "\\";
        private final Path path;

        Default(final String first, final String... more) {
            this(Path.of(first, more));
        }

        Default(final Path path) {
            this.path = path;
        }

        @Override
        public int numberOfParts() {
            return this.path.getNameCount();
        }

        @Override
        public PartPath partFor(final int index) {
            return new Default(this.path.getName(index));
        }

        @Override
        public PartPath subpath(final int beginIndex, final int endIndex) {
            return new Default(this.path.subpath(beginIndex, endIndex));
        }

        @Override
        public PartPath normalized() {
            return new Default(this.path.normalize());
        }

        @Override
        public PartPath resolve(final PartPath other) {
            return new Default(this.path.resolve(other.asPath()));
        }

        @Override
        public PartPath resolve(final String other) {
            return new Default(this.path.resolve(other));
        }

        @Override
        public PartPath relativize(final PartPath other) {
            return new Default(this.path.relativize(other.asPath()));
        }

        @Override
        public Path asPath() {
            return this.path;
        }

        @Override
        public String asString() {
            final String s;
            if (BACKSLASH.equals(this.path.getFileSystem().getSeparator())) {
                s = this.path.toString().replace(BACKSLASH, SLASH);
            } else {
                s = this.path.toString();
            }
            return s;
        }

        @Override
        public String toString() {
            return asString();
        }
    }
}
