/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import java.util.Objects;

final class Metadata {
    private final String worksheetLocalisedName;
    private final CellReferencesRange cellReferencesRange;
    private final String name;

    Metadata(final String worksheetLocalisedName, final CellReferencesRange cellReferencesRange, final String name) {
        this.worksheetLocalisedName = worksheetLocalisedName;
        this.cellReferencesRange = cellReferencesRange;
        this.name = name;
    }

    String worksheetLocalisedName() {
        return this.worksheetLocalisedName;
    }

    CellReferencesRange cellReferencesRange() {
        return this.cellReferencesRange;
    }

    String name() {
        return this.name;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Metadata that = (Metadata) o;
        return worksheetLocalisedName.equals(that.worksheetLocalisedName) && cellReferencesRange.equals(that.cellReferencesRange);
    }

    @Override
    public int hashCode() {
        return Objects.hash(worksheetLocalisedName, cellReferencesRange);
    }
}
