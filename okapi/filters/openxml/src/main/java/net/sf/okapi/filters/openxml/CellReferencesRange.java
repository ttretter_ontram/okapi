/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

final class CellReferencesRange {
    private static final char COLUMN_INDEX_PART_MINIMUM = 'A';
    private static final char COLUMN_INDEX_PART_MAXIMUM = 'Z';
    private static final String DELIMITER = ":";
    private final String string;
    private CellReference first;
    private CellReference last;
    private boolean split;
    private Set<Integer> rows;
    private Set<String> columns;

    CellReferencesRange(final CellReference cellReference) {
        this(cellReference, cellReference);
    }

    CellReferencesRange(final CellReference first, final CellReference last) {
        this(first.toString() + DELIMITER + last.toString());
        this.first = first;
        this.last = last;
        this.split = true;
    }

    CellReferencesRange(final String string) {
        this.string = string;
    }

    CellReference first() {
        if (!this.split) {
            split();
        }
        return this.first;
    }

    CellReference last() {
        if (!this.split) {
            split();
        }
        return this.last;
    }

    private void split() {
        String[] references = this.string.split(DELIMITER);
        if (2 < references.length) {
            throw new IllegalStateException("Unexpected merged cell range: ".concat(this.string));
        } else if (1 == references.length) {
            this.first = new CellReference(this.string);
            this.last = new CellReference(this.string);
        } else {
            this.first = new CellReference(references[0]);
            this.last = new CellReference(references[1]);
        }
        this.split = true;
    }

    Set<Integer> rows() {
        if (null == this.rows) {
            var firstRow = first().row();
            var lastRow = last().row();
            if (firstRow == lastRow) {
                this.rows = Set.of(firstRow);
            } else {
                this.rows = IntStream.rangeClosed(firstRow, lastRow)
                        .boxed()
                        .collect(Collectors.toSet());
            }
        }
        return this.rows;
    }

    Set<String> columns() {
        if (null == this.columns) {
            String firstColumnIndex = this.first().column();
            String lastColumnIndex = this.last().column();

            if (firstColumnIndex.equals(lastColumnIndex)) {
                this.columns = Set.of(firstColumnIndex);
            } else {
                this.columns = new LinkedHashSet<>();
                String columnIndex = firstColumnIndex;
                this.columns.add(columnIndex);
                while (!columnIndex.equals(this.last().column())) {
                    columnIndex = incrementColumnIndex(columnIndex);
                    this.columns.add(columnIndex);
                }
            }
        }
        return columns;
    }

    private static String incrementColumnIndex(final String columnIndex) {
        return incrementColumnIndexPart(columnIndex.toCharArray(), columnIndex.length() - 1);
    }

    private static String incrementColumnIndexPart(char[] columnIndexParts, int partPosition) {
        if (0 > partPosition) {
            return COLUMN_INDEX_PART_MINIMUM + new String(columnIndexParts);
        }
        char part = columnIndexParts[partPosition];
        if (COLUMN_INDEX_PART_MAXIMUM == part) {
            columnIndexParts[partPosition] = COLUMN_INDEX_PART_MINIMUM;
            return incrementColumnIndexPart(columnIndexParts, --partPosition);
        }
        columnIndexParts[partPosition] = ++part;
        return new String(columnIndexParts);
    }

    boolean partialMatch(final Set<Integer> rows, final Set<String> columns) {
        if (rows.isEmpty() && columns.isEmpty()) {
            return true;
        }

        long rowMatchesCount = rowMatchesCount(rows);
        long columnMatchesCount = columnMatchesCount(columns);

        int rowsSize = rows().size();
        int columnsSize = columns().size();

        if (rowsSize == rowMatchesCount || columnsSize == columnMatchesCount) {
            // fully matches rows or columns
            return false;
        }
        return rowsSize > rowMatchesCount || columnsSize > columnMatchesCount;
    }

    boolean anyMatch(final Set<Integer> rows, final Set<String> columns) {
        final boolean rowsMatched = rows().stream().anyMatch(rows::contains);
        final boolean columnsMatched = columns().stream().anyMatch(columns::contains);
        return rowsMatched || columnsMatched;
    }

    boolean anyMatch(final CellReferencesRange cellReferencesRange) {
        return anyMatch(cellReferencesRange.rows(), cellReferencesRange.columns());
    }

    Set<Integer> rowMatches(final Set<Integer> rows) {
        LinkedHashSet<Integer> matches = new LinkedHashSet<>(rows.size());
        if (rows.isEmpty()) {
            return matches;
        }

        for (Integer i : rows()) {
            if (rows.contains(i)) {
                matches.add(i);
            }
        }
        return matches;
    }

    private long rowMatchesCount(final Set<Integer> rows) {
        if (rows.isEmpty()) {
            return 0;
        }

        long i = 0;
        for (var r : rows()) {
            if (rows.contains(r)) {
                i++;
            }
        }
        return i;
    }

    Set<String> columnMatches(final Set<String> columns) {
        LinkedHashSet<String> matches = new LinkedHashSet<>(columns.size());
        if (columns.isEmpty()) {
            return matches;
        }

        for (String s : columns()) {
            if (columns.contains(s)) {
                matches.add(s);
            }
        }
        return matches;
    }

    private long columnMatchesCount(final Set<String> columns) {
        if (columns.isEmpty()) {
            return 0;
        }

        long i = 0;
        for (var c : columns()) {
            if (columns.contains(c)) {
                i++;
            }
        }
        return i;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final CellReferencesRange that = (CellReferencesRange) o;
        return this.string.equals(that.string);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.string);
    }

    @Override
    public String toString() {
        return this.string;
    }
}
