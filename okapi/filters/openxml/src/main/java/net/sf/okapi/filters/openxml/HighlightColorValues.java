/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import java.util.Arrays;
import java.util.List;

public final class HighlightColorValues implements PresetColorValues {
    private final PresetColorValues presetColorValues;

    public HighlightColorValues() {
        this(
            new PresetColorValues.Default(
                Arrays.asList(
                    new Color.Default.Value("black", "Black", "000000"),
                    new Color.Default.Value("blue", "Blue", "0000FF"),
                    new Color.Default.Value("cyan", "Cyan", "00FFFF"),
                    new Color.Default.Value("green", "Green", "00FF00"),
                    new Color.Default.Value("magenta", "Magenta", "FF00FF"),
                    new Color.Default.Value("red", "Red", "FF0000"),
                    new Color.Default.Value("yellow", "Yellow", "FFFF00"),
                    new Color.Default.Value("white", "White", "FFFFFF"),
                    new Color.Default.Value("darkBlue", "Dark Blue", "000080"),
                    new Color.Default.Value("darkCyan", "Dark Cyan", "008080"),
                    new Color.Default.Value("darkGreen", "Dark Green", "008000"),
                    new Color.Default.Value("darkMagenta", "Dark Magenta", "800080"),
                    new Color.Default.Value("darkRed", "Dark Red", "800000"),
                    new Color.Default.Value("darkYellow", "Dark Yellow", "808000"),
                    new Color.Default.Value("darkGray", "Dark Gray", "808080"),
                    new Color.Default.Value("lightGray", "Light Gray", "C0C0C0"),
                    new Color.Default.Value("none", "None", "")
                )
            )
        );
    }

    HighlightColorValues(final PresetColorValues presetColorValues) {
        this.presetColorValues = presetColorValues;
    }

    @Override
    public List<String> externalNames() {
        return this.presetColorValues.externalNames();
    }

    @Override
    public Color.Value valueFor(final String value) {
        return this.presetColorValues.valueFor(value);
    }
}
