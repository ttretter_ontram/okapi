/*
 * =============================================================================
 *   Copyright (C) 2010-2019 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

interface Namespaces2 {
    Iterator<Namespace> iterator();
    Namespaces2 with(final String prefix);
    void readWith(final XMLEventReader eventReader) throws XMLStreamException;

    class Default implements Namespaces2 {
        private List<Namespace> namespaces;

        Default() {
            this(new LinkedList<>());
        }

        Default(final List<Namespace> namespaces) {
            this.namespaces = namespaces;
        }

        @Override
        public Iterator<Namespace> iterator() {
            return this.namespaces.iterator();
        }

        @Override
        public Namespaces2 with(final String prefix) {
            return new Default(
                this.namespaces.stream()
                    .filter(n -> n.prefix().equals(prefix))
                    .collect(Collectors.toList())
            );
        }

        @Override
        public void readWith(final XMLEventReader eventReader) throws XMLStreamException {
            while (eventReader.hasNext()) {
                final XMLEvent event = eventReader.nextEvent();
                if (event.isStartElement()) {
                    readFrom(event.asStartElement());
                    break;
                }
            }
        }

        private void readFrom(final StartElement startElement) {
            final Iterator<javax.xml.stream.events.Namespace> iterator = startElement.getNamespaces();
            while (iterator.hasNext()) {
                final javax.xml.stream.events.Namespace namespace = iterator.next();
                this.namespaces.add(
                    new Namespace.Default(namespace.getPrefix(), namespace.getNamespaceURI())
                );
            }
        }
    }
}
