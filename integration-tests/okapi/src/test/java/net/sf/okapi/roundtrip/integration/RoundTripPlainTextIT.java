package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.common.integration.XmlOrTextRoundTripIT;
import net.sf.okapi.filters.plaintext.PlainTextFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class RoundTripPlainTextIT extends XmlOrTextRoundTripIT {
	private static final String CONFIG_ID = "okf_plaintext";
	private static final String DIR_NAME = "/plaintext/";
	private static final List<String> EXTENSIONS = Arrays.asList(".txt");

	public RoundTripPlainTextIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, PlainTextFilter::new);
		addKnownFailingFile("BOM_MacUTF16withBOM2.txt");
	}

	@Test
	public void plainTextFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.Utf8FilePerLineComparator());
	}

	@Test
	public void plainTextSerializedFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(false, new FileComparator.Utf8FilePerLineComparator());
	}
}
