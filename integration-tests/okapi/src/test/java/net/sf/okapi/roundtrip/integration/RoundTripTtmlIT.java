package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.ttml.TTMLFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class RoundTripTtmlIT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_ttml";
	private static final String DIR_NAME = "/ttml/";
	private static final List<String> EXTENSIONS = Arrays.asList(".ttml");

	public RoundTripTtmlIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, TTMLFilter::new);
	}

	@Test
	public void ttmlFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(false);
		realTestFiles(false, new FileComparator.EventComparator());
	}

	@Test
	public void ttmlSerializedFiles() throws FileNotFoundException, URISyntaxException {
		setSerializedOutput(true);
		realTestFiles(false, new FileComparator.EventComparator());
	}
}
