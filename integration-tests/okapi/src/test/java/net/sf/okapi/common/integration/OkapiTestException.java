package net.sf.okapi.common.integration;

import net.sf.okapi.common.exceptions.OkapiException;

public class OkapiTestException extends OkapiException {

	public OkapiTestException(String message, Throwable cause) {
		super(message, cause);
	}
}
