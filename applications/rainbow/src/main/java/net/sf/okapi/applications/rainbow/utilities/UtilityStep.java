/*===========================================================================
  Copyright (C) 2008-2009 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.applications.rainbow.utilities;

import java.io.File;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.pipeline.IPipelineStep;

public class UtilityStep implements IPipelineStep {

	private final IFilterDrivenUtility utility;

	public UtilityStep (IFilterDrivenUtility utility) {
		this.utility = utility;
	}

	@Override
	public Event handleEvent (Event event) {
		utility.handleEvent(event);
		return event;
	}
	
	@Override
    public String getHelpLocation () {
		return ".." + File.separator + "help" + File.separator + "steps";
	}

	@Override
    public void cancel () {
		// Cancel needed here
	}

	@Override
    public String getName () {
		return utility.getName();
	}

	@Override
    public String getDescription () {
		// TODO: Implement real descriptions
		return utility.getName();
	}
	
	public void pause () {
	}

	public void postprocess () {
		utility.postprocess();
	}

	public void preprocess () {
		utility.preprocess();
	}

	@Override
	public void destroy () {
		utility.destroy(); 
	}

	public boolean hasNext () {
		return false;
	}

	
	//======================= Just temporary until we move all utilities to steps
	
	@Override
    public IParameters getParameters() {
		//x TODO Auto-generated method stub
		return null;
	}

	@Override
    public void setParameters(IParameters params) {
		//x TODO Auto-generated method stub		
	}

	@Override
	public boolean isDone() {
		//x TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isLastOutputStep() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setLastOutputStep(boolean isLastStep) {
		// TODO Auto-generated method stub		
	}
}
