# Changes from M17 to M18

## Applications

* Rainbow

    * Changed the batch mode so the log and error output go to the
      rainbowBatchLog.txt file in the user home directory.
    * Added the `-rd` option (root directory) to the
      command-line mode.

* Tikal

    * Resolved [issue #252](https://bitbucket.org/okapiframework/okapi/issues/252): Added `-rd` option to set the
      root directory (Patch from Mihai)
    * Fixed [issue #254](https://bitbucket.org/okapiframework/okapi/issues/254): Added support to change the display encoding
      using the `.Tikal` configuration file and the `displayEncoding`
      setting.
    * Fixed [issue #213](https://bitbucket.org/okapiframework/okapi/issues/213): Added the `-noalttrans` option for
      the "Leverage Files From Moses" command.

* CheckMate

    * Added several features (See Library / Checker below for details).

* Longhorn

    * Made API available to execute pipelines with multiple target
      languages.

## Filters

* RainbowKit Filter

    * Added pre-defined configuration "noprompt" for processing
      without prompting.

* XML Filter

    * Added implementation for the ITS 2.0 Domain data category.
    * Added implementation for the ITS 2.0 External Resources
      Reference data category.
    * Added implementation for the ITS 2.0 Locale Filter data
      category.
    * Added implementation for the ITS 2.0 Preserve Space data
      category.
    * Added implementation for the ITS 2.0 Storage Size data
      category.

* XLIFF Filter

    * Added support for the `maxbytes` and ITS Storage
      Size data category.

* MIF Filter

    * Fixed: For MIF 10 files, the wrong encoding was used to read
      the files.

* TMX Filter

    * Added option to set TextUnit Segmentation flag based on the
      filter configuration. \
      **Important: This introduce a change in behavior. Now, by
      default, TMX entries that are set to `segType="segment"` or not
      set at all will be marked as segment in the extracted
      resource. this may affect some output, for example XLIFF,
      where such entries will be noted as segmented as well.**

## Steps

* Batch Translation Step

    * Fixed [issue #260](https://bitbucket.org/okapiframework/okapi/issues/260) where an error occured at the end of the
      batch process when the option 'send TMX to the next step' was
      not set.

* Translation Comparison Step

    * Added support for `${rootDir}`, `${inputRootDir}`
      and locale variables.

* Format Conversion Step

    * Added the UI for the option "Do not output entries without
      text".

* Segmentation and Desegmentation Step

    * Will now affect all target locales, that you are processing.

## Connectors

* Microsoft Translator Connector

    * Updated code and interface to use Azure Marketplace Access
      Tokens instead of AppIDs.

## Libraries

* Upgraded the SWT library from 3.7 to 4.2.

* XLIFFWriter

    * Added support for `its:domain` and `its:externalResourcesRef`
      properties

* Checker

    * Added XLIFF validation against XLIFF schema (`xliff-core-1.2-transitional.xsd`)
      (feature, [issue #263](https://bitbucket.org/okapiframework/okapi/issues/263))
    * Adding the ability to generate XML report ([feature, issue #264](https://bitbucket.org/okapiframework/okapi/issues/264))
    * Fixed [issue #257](https://bitbucket.org/okapiframework/okapi/issues/257): different leading/trailing spaces does not
      detect inconsistent non-breaking spaces.
    * Added support for validating the ITS Storage Size property for
      text unit.
    * Removed extra files leftover from xliff-lib and olifant migration.
    * Added the `TARGET_LOCALES` step parameter and depracted the
      `TARGET_LOCALE` one.
    * `ITSEngine` class for ITS 2.0:
        * Added support for the Target Pointer data category.
        * Added support for the Domain data category.
        * Added support for the External Resources Reference data
          category.
        * Added support for the Locale Filter data category.
        * Added support for the Preserve Space data category.
        * Added support for the Localization Quality Issue data
          category.
        * Added support for the Storage Size data category.
    * Added options to pre- and post-process text that is being
      letter-coded to prevent interference with letter-decoding.

## Trados Utilities Plugin

* Added the [Trados/Word steps](http://www.opentag.com/okapi/wiki/index.php?title=Trados_Utilities_Plugin) to the main build.

